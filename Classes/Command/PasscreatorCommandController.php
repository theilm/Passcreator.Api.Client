<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 21.11.17
 * Time: 11:07
 */

namespace Passcreator\Api\Client\Command;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Cli\CommandController;
use Passcreator\Api\Client\Domain\Service\PasscreatorService;

class PasscreatorCommandController extends CommandController
{

    /**
     * @var PasscreatorService
     * @Flow\Inject
     */
    protected $passcreatorService;

    /**
     * @param string $templateIdentifier
     * @param string $content
     */
    public function createPassCommand($templateIdentifier, $content) {
        $contentArray = json_decode($content, true);

        $response = $this->passcreatorService->createPass($templateIdentifier, $contentArray);
        $this->outputLine(\Neos\Flow\var_dump($response, 'Pass creation response'));
    }

    public function listTemplatesCommand()
    {
        $templates = $this->passcreatorService->getTemplates();
        foreach ($templates as $template) {
            $this->outputLine('%-70s %s',
                ['Name: <comment>' . $template['name'] . '</comment>', 'Identifier: ' . $template['identifier']]);
        }
    }

    /**
     * @param string $templateIdentifier
     */
    public function templateInfoCommand($templateIdentifier)
    {
        $info = $this->passcreatorService->getTemplateInfo($templateIdentifier);
        $this->outputLine(\Neos\Flow\var_dump($info, 'Information for template with ID ' . $templateIdentifier));
    }

    /**
     * @param string $templateIdentifier
     */
    public function templateFieldsCommand($templateIdentifier) {
        $info = $this->passcreatorService->getTemplateInfo($templateIdentifier, true);
        $this->outputLine(\Neos\Flow\var_dump($info, 'Information for template with ID ' . $templateIdentifier));
    }
}