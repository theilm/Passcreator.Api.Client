<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 21.11.17
 * Time: 11:13
 */

namespace Passcreator\Api\Client\Exception;


use Neos\Flow\Exception;

class PasscreatorException extends Exception
{

}