<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 21.11.17
 * Time: 09:57
 */

namespace Passcreator\Api\Client\Domain\Service;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Http\Client\CurlEngine;
use Neos\Flow\Http\Request;
use Neos\Flow\Http\Uri;
use Passcreator\Api\Client\Exception\PasscreatorException;
use Passcreator\Api\Client\Exception\ResourceNotFoundException;
use Passcreator\Api\Client\Exception\UnauthorizedException;

class PasscreatorService
{

    /**
     * @Flow\InjectConfiguration(package="Passcreator.Api.Client")
     * @var array
     */
    protected $settings;

    /**
     * @var CurlEngine
     * @Flow\Inject
     */
    protected $requestEngine;

    /**
     * @param string $templateId
     * @param array $content
     * @param bool $zapierStyle
     * @return array
     */
    public function createPass($templateId, $content = array(), $zapierStyle = false)
    {
        return $this->post('pass?passtemplate=' . $templateId . ($zapierStyle ? '&zapierStyle=true' : ''), $content);
    }

    /**
     * @param $passId
     * @param array $content
     * @param bool $zapierStyle
     * @return array
     */
    public function updatePass($passId, $content = array(), $zapierStyle = false)
    {
        return $this->post('pass/' . $passId . ($zapierStyle ? '&zapierStyle=true' : ''), $content);
    }

    /**
     * @param $passId
     * @return array
     */
    public function deletePass($passId)
    {
        return $this->delete('pass/' . $passId);
    }

    /**
     * Example input:
     * {
     *  "name": "Test API",
     *  "passTemplateId": "",
     *  "type": 0,
     *  "scanMode": 1,
     *  "additionalProperties": [
     *    {
     *     "type": "boolean",
     *     "name": "Allowed to send marketing mails?"
     *    },
     *    {
     *     "type": "unicode",
     *     "name": "Tell us about you"
     *    },
     *    {
     *     "type": "double",
     *     "name": "Transaction value"
     *   }
     *  ]
     * }
     *
     * @param array $content
     * @return array
     */
    public function createAppConfiguration($content = array())
    {
        return $this->post('appconfiguration', $content);
    }

    /**
     * @param string $searchString
     * @param string $templateId
     * @return array
     */
    public function searchPasses($searchString, $templateId)
    {
        return $this->get('pass/search/' . $templateId . '/' . $searchString);
    }

    /**
     * @param string $passId
     * @param bool $voided
     * @return array
     */
    public function markPassVoided($passId, $voided = true)
    {
        return $this->put('pass/' . $passId, array('voided' => $voided));
    }

    /**
     * @param $link
     * @return array
     */
    public function renewAppConfigurationLink($link)
    {
        return $this->post('appconfigurationlink/renew?link=' . $link);
    }

    /**
     * @return array
     */
    public function getTemplates()
    {
        return $this->get('pass-template');
    }

    /**
     * @param string $templateId
     * @param bool $zapierStyle
     * @return array
     */
    public function getTemplateInfo($templateId, $zapierStyle = false)
    {
        return $this->get('pass-template/' . $templateId . ($zapierStyle ? '?zapierStyle=true' : ''));
    }

    /**
     * @param array $content
     */
    public function subscribeRestHook($content = array())
    {
        return $this->post('hook/subscribe/', $content);
    }

    /**
     * @param $resource
     * @param null $arguments
     * @return array
     */
    private function delete($resource, $arguments = null)
    {
        return $this->makeRequest('DELETE', $resource, $arguments);
    }

    /**
     * @param string $resource The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be send to the API endpoint
     * @return array
     */
    private function get($resource, array $arguments = null)
    {
        return $this->makeRequest('GET', $resource, $arguments);
    }

    /**
     * @param string $resource The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be send to the API endpoint
     * @return array
     */
    private function put($resource, array $arguments = null)
    {
        return $this->makeRequest('PUT', $resource, $arguments);
    }

    /**
     * @param string $resource The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be send to the API endpoint
     * @return array
     */
    private function post($resource, array $arguments = null)
    {
        return $this->makeRequest('POST', $resource, $arguments);
    }

    /**
     * @param string $method The HTTP method
     * @param string $resource The REST resource name (e.g. "lists")
     * @param array|null $arguments Arguments to be send to the API endpoint
     * @return array The decoded response
     */
    private function makeRequest($method, $resource, array $arguments = null)
    {
        $uri = new Uri($this->settings['apiEndpoint'] . '/' . $resource);
        if ($method === 'GET' && $arguments !== null) {
            $uri->setQuery(http_build_query($arguments));
        }
        $request = Request::create($uri, $method);
        $request->setHeader('Accept', 'application/json');
        $request->setHeader('Content-Type', 'application/json');
        $request->setHeader('Authorization', $this->settings['apiKey']);
        if ($method !== 'GET' && $arguments !== null) {
            $request->setContent(json_encode($arguments));
        }

        $response = $this->requestEngine->sendRequest($request);
        $decodedBody = json_decode($response->getContent(), true);
        if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
            return $decodedBody;
        }
        $errorMessage = isset($decodedBody['detail']) ? $decodedBody['detail'] : $response->getStatusCode() . ' Unknown error.';
        if ($response->getStatusCode() === 404) {
            throw new ResourceNotFoundException($errorMessage, 1483538558);
        }

        if ($response->getStatusCode() === 401) {
            throw new UnauthorizedException($errorMessage, 1483538559);
        }

        throw new PasscreatorException('Unknown error. Status code: ' . $response->getStatusCode(), 1483538559);
    }

}