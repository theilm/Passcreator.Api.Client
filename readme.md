
[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

# Package to connect to the Passcreator API

This [Flow](https://flow.neos.io/) package allows you to connect to the [Passcreator API](http://developer.passcreator.com).


## Installation

To install the package just do a

```bash
$ composer require passcreator/api-client
```

## Configuration

Add the API key of your Passcreator account to your Settings.yaml
```yaml
Passcreator:
  Api:
    Client:
      apiKey: 'abcde12345cwe'
```

## How to use it

Use the PasscreatorService class of this package to execute actions against the Passcreator API.

```php
    /**
     * @var \Passcreator\Api\Client\Domain\Service\PasscreatorService
     * @Flow\Inject
     */
    protected $passcreatorService;
```

The following endpoints are currently implemented in this package:
 - Creating passes
 - Deleting passes
 - Updating passes
 - Searching for passes using a search phrase
 - Mark a pass voided or unvoid it
 - List pass-templates
 - Get information about a pass-template and its' fields

We'll make sure to update the package as we move on. Currently not all features of the Passcreator API are implemented here.
If you need additional endpoints, drop us a message. We'll be happy to help.
